#include <bits/stdc++.h>
#include "iostream"
#define FILE_PTAH  "E:\\Users\\asus\\Desktop\\books.txt"

using namespace std;
class book{
    public:
        void defining(vector <string> inputs);
        string Title;
        string Shelf_Number;
        string Authors;
        string Edition;
        string Publisher;
        string Published_year;
        string ISBN;
        string Length;
        string Subjects;
        vector<string> status = {"NB" , "-1"};
};

class user{
    public:
        void defining(vector <string> inputs);
        string username;
        string password;
        string firstname;
        string lastname;
        string birth_date;
        vector<book> borrowed_books;

};
void user :: defining(vector<string> inputs)
{
    username = inputs[0];
    password = inputs[1];
    firstname = inputs[2];
    lastname = inputs[3];
    birth_date = inputs[4];
}

void book :: defining(vector<string> inputs)
{
   Title = inputs[0];
   Shelf_Number= inputs[1];
   Authors = inputs[2];
   Edition = inputs[3];
   Publisher = inputs[4];
   Published_year = inputs[5];
   ISBN = inputs[6]; 
   Length = inputs[7];
   Subjects = inputs[8];
}


vector<string> split_line(string s){
    stringstream ss(s);
    vector<string>tokens;
    string temp;
    while(getline(ss,temp,' ')){
        tokens.push_back(temp);
    }
    return tokens;
}
/*void read_txt(string path , vector <book> &all_books){
    ifstream file(FILE_PTAH, ios::in);
    string s;
    while(getline(file, s)){
        vector<string> one_book_data = split_line(s);
        book new_book;
        new_book.defining(one_book_data);
        all_books.push_back(new_book);
    }
}*/

int menu()
{
    int command;
    cout << "login: 1 \nsignup: 2\nexit: 3\n";
    cin >> command;
    return command;
}
vector <string> user_inputs()
{
    vector<string> data = {"username" , "password" , "firstname" , "lastname" , "birthdate"};
    vector<string> all_inputs;
    for(int i = 0 ; i < 5 ; i++)
    {
        cout << "enter your " << data[i] << ":";
        string x;
        cin >> x;
        all_inputs.push_back(x);
    }
    return all_inputs;
}
bool check_username(vector <user> all_users , string username)
{
    for(int i = 0 ; i < all_users.size() ; i++)
    {
        if(all_users[i].username == username)
            return false;
    }
    return true;
}
int check_existing(vector <user> all_users , vector<string> inputs)
{
    for (int i = 0 ; i < all_users.size() ; i++)
    {
        if(all_users[i].username == inputs[0] && all_users[i].password == inputs[1])
            return i;
    }
    return -1;
}
int check_book(vector <book> all_books , int search_base , string x)
{
    for (int i = 0 ; i < all_books.size() ; i++)
    {
        if(search_base == 1)
            if(all_books[i].Title == x)
                return i;
        if(search_base == 2)
            if(all_books[i].Authors == x)
                return i;
        if(search_base == 3)
            if(all_books[i].Publisher == x)
                return i;
        if(search_base == 4)
            if(all_books[i].Published_year == x)
                return i;
    }
    return -1;
}
void search_and_borrow(vector <user> &all_users , int id , vector <book> &all_books)
{
    vector<string> based = {"title" , "author" , "publisher" , "published year"};
    int search_base;
    cout << "base on:\ntitle=1\nauthor=2\npublisher=3\npublished year=4\n";
    cin >> search_base;
    cout << "enter " << based[search_base-1] <<":";
    string x;
    cin >> x;
    int where_book_is = check_book(all_books, search_base , x);
    if(where_book_is != -1)
    {
        cout << "this book does exist\n";
        if(all_books[where_book_is].status[0] == "B" || all_users[id].borrowed_books.size() >= 2)
            cout << "you cant borrow this book\n";
        if(all_books[where_book_is].status[0] == "NB" && all_users[id].borrowed_books.size() < 2)
        {
            cout << "you can borrow this book , if you want enter 1 , else enter a key:";
            int command;
            cin >> command;
            if(command == 1)
                {
                    all_users[id].borrowed_books.push_back(all_books[where_book_is]);
                    all_books[where_book_is].status = {"B" , to_string(id)};
                    cout << "you borrowed this book\n";
                }   
        }
     }
    else
        cout << "this book does not exist\n";
}

void refund(vector <user> &all_users , int id , vector <book> &all_books)
{
    if(all_users[id].borrowed_books.size() != 0)
    {
        for(int j = 0 ; j< all_users[id].borrowed_books.size() ; j++)
            cout << j+1 << "-" << "you have " << all_users[id].borrowed_books[j].Title << "\n";
        cout << "which one do you want to refund:";
        int x;
        cin >> x;
        int book_place = check_book(all_books , 1 , all_users[id].borrowed_books[x-1].Title);
        all_books[book_place].status = {"NB" , "-1"};
        all_users[id].borrowed_books.erase(all_users[id].borrowed_books.begin() + x - 1);
        cout << "book deleted sucessfuly\n"; 
    }
    else
        cout << "you dont have any books\n";   
}
void user_pannel(vector <user> &all_users , int id , vector <book> &all_books)
{
    while(true)
    {
        cout << "pannel:\nsearch for a book= 1\nrefund a book= 2\nexit= 3\n";
        int command;
        cin >> command;
        if(command == 1)
           search_and_borrow(all_users , id , all_books);
        if(command == 2)
        {
            refund(all_users,id,all_books);
        }
        if(command == 3)
            return;
    }
}
int main(){
    vector<user> all_users;
    vector<book> all_books;
    //read_txt(FILE_PTAH , all_books); 
    while(true)
    {
        int command = menu();
        if(command == 2)
        {
            user new_user;
            vector<string> inputs = user_inputs();
            if(check_username(all_users , inputs[0]) == true)
            {
                new_user.defining(inputs);
                all_users.push_back(new_user);
                system("cls");
                cout << "sucessfull\n";
            }
            else
            {
                system("cls");
                cout << "this username already exists\n";
            }
        }
        if(command == 1)
        {   
            vector<string> inputs = {"" , ""};
            cout << "enter your username:";
            cin >> inputs[0];
            cout << "enter your password:";
            cin >> inputs[1];
            int id = check_existing(all_users , inputs);
            if(id != -1)
                user_pannel(all_users , id , all_books);
            else
                cout << "your credentials don't match\n";
        }
        if(command == 3)
            return 0;
    }
    return 0;
    }
